# node-red-automations

## Current automations-
- Check bus timings when I leave the house
- 

## In use NodeRED nodes-
- node-red-contrib-home-assistant (https://flows.nodered.org/node/node-red-contrib-home-assistant)
- node-red-contrib-alexa https://flows.nodered.org/node/node-red-contrib-alexa
- node-red-contrib-alexa-home-skill https://flows.nodered.org/node/node-red-contrib-alexa-home-skill
- node-red-contrib-cheerio https://flows.nodered.org/node/node-red-contrib-cheerio
- node-red-contrib-telegrambot https://flows.nodered.org/node/node-red-contrib-telegrambot
- node-red-contrib-time-range-switch https://flows.nodered.org/node/node-red-contrib-time-range-switch

